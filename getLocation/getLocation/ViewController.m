//
//  ViewController.m
//  getLocation
//
//  Created by Prince on 26/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController (){
     CLLocationManager *manager;
}
@property (strong, nonatomic) IBOutlet UILabel *lati;
@property (strong, nonatomic) IBOutlet UILabel *longi;
@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (strong, nonatomic) IBOutlet UIButton *getLocation;
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;

@end

@implementation ViewController
@synthesize appleMap;
@synthesize textField1;
@synthesize textField2;

- (void)viewDidLoad {
    
    
   

    
    
    
    
   
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)getLocationPressed:(id)sender {
    
    
    [manager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
    {
        NSLog(@"didFailWithError: %@", error);
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
    }
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
       self.textField1.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
       self.textField2.text= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = currentLocation.coordinate;
        annotationPoint.title = @"Here";
        annotationPoint.subtitle = @"you are";
        [appleMap addAnnotation:annotationPoint];

        
    }
   
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
